=== Woocommerce Category Banner Management ===
Plugin Name: Woocommerce Category Banner Management
Plugin URI: http://multidots.com/
Author: Multidots
Author URI: http://multidots.com/
Contributors: dots, thedotstore
Stable tag: 1.0.3
Tags: banner, banner management woocommerce banner management, banner in cart, banner in shop, banner in checkout, banner in thank you, banner in category
Requires at least: 2.1
Tested up to: 4.7
Donate link: 
Copyright: (c) 2015-2016 Multidots Solutions PVT LTD (info@multidots.com) 
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

With this plugin, You can easily add banner in WooCommerce stores and you can upload the banner  specific for page,category  and welcome page.

== Description ==

Banner Management for WooCommerce plugin that allows you to manage page and category wise banners in your WooCommerce store.You can easily add banner in WooCommerce stores and you can upload the banner specific for page and category.

Pro Plugin Demo : <a href ="http://woocategorybannermanagement.demo.store.multidots.com/" target="_blank">View Demo</a>

= Plugin Functionality: =
   * Easy to use
   * Enable and disable banner for particular page, category
   * you can Manage " Page Specific Banner" and " Category Specific Banner "
   * Add Banner for Shop Page
   * Add Banner for Cart Page
   * Add Banner for Checkout Page 
   * Add Banner for Thank you page
   * you can add Banner URL/LINK for a particular banner .
   * Fully tested with latest version of WooCommerce. 
   
   
= Woocommerce Category Banner Management Pro version: = 

Need even more? upgrade to <a href ="https://store.multidots.com/woocommerce-category-banner-management">Woocommerce Category Banner Management</a> and get all the features available in Woocommerce Category Banner Management.

Woocommerce Category Banner Management plugin that allows you to manage page and category wise banners in your WooCommerce store.You can easily add single banner and Multiple banner for page and category in WooCommerce stores.

= Key Features of >Woocommerce Category Banner Management = 

= Manage Page Specific Banner = 

Using this feature you can add a single and multiple banners and assigned to a specific page and category of your WooCommerce store. Like,

* Banner for Shop page
* Banner for Cart page
* Banner for Checkout page
* Banner for Thank you page

= Add single Banner for specific page = 

Allows you to add a single banner image to a specific page of the website.You can configure banner for particular time periods and add custom link of that banner also.

= Add Multiple Banner for specific page = 

Allows you to add a Multiple banner image to a specific page of the website. you can configure banner for particular time periods and add custom link of that banner.

= Display Random and Slider Banner for specific page and category = 

Displays the Random banners that you specify by the order of their position every time the page is refreshed.Also plugin allows you to display simple image slider with enable/ disable navigation option and assigned to a specific page and category of your WooCommerce store

= Add Category Banner = 

Using this feature you can easily add category wise banner and link at the top of your product category page. Easily update the image through your product category edit page.

We always welcome user suggestions. Let us know what you think about this plugin you liked or may have disliked. Users feedback is important for us to improve more our plugin.


= You can check our other plugins: =

1. <a href ="https://store.multidots.com/go/flat-rate">Advance Flat Rate Shipping Method For WooCommerce</a>
2. <a href ="https://store.multidots.com/go/dotstore-woocommerce-blocker">WooCommerce Blocker – Prevent Fake Orders And Blacklist Fraud Customers</a>
3. <a href ="https://store.multidots.com/go/dotstore-enhanced-ecommerce-tracking">WooCommerce Enhanced Ecommerce Analytics Integration With Conversion Tracking</a>
4. <a href ="https://store.multidots.com/go/dotstore-woo-category-banner">Woocommerce Category Banner Management</a>
5. <a href ="https://store.multidots.com/go/dotstore-woo-extra-fees">Woocommerce Conditional Extra Fees</a>
6. <a href ="https://store.multidots.com/go/dotstore-woo-product-sizechart">Woocommerce Advanced Product Size Charts</a>
7. <a href ="https://store.multidots.com/go/dotstore-admenumanager-wp">Advance Menu Manager for WordPress</a>
8. <a href ="https://store.multidots.com/go/dotstore-woo-savefor-later">Woocommerce Save For Later Cart Enhancement</a>
9. <a href ="https://store.multidots.com/go/brandagency">Brand Agency- One Page HTML Template For Agency,Startup And Business</a>
10. <a href ="https://store.multidots.com/go/Meraki">Meraki One Page HTML Resume Template</a>
11. <a href ="https://store.multidots.com/go/dotstore-aapify-theme">Appify - Multipurpose One Page Mobile App landing page HTML</a>

== Installation ==

* Download the plugin
* Upload the folder "banner-management-for-woocommerce" to wp-content/plugins (or upload a zip through the Wordpress admin)
* Activate and enjoy!

== Frequently Asked Questions ==

== In which WordPress version this Plugin is compatible?

It is compatible from 2.1 to 4.7 WordPress version.

== In which WooCommerce version this Plugin is compatible? ==

It is compatible for 2.6 and greater than WooCommerce plugin

== Screenshots ==
1. Banner Management for WooCommerce 1.
2. Banner Management for WooCommerce 2.
3. Banner Management for WooCommerce 3.
4. Banner Management for WooCommerce 4.

== Upgrade Notice ==

Automatic updates should work great for you.  As always, though, we recommend backing up your site prior to making any updates just to be sure nothing goes wrong.
 

== Changelog ==

= 1.0.3 - 26.12.2016 =
* Compatible with WordPress Version 4.7
* Compatible with WooCommerce Version 2.6.11
* Checkout Page Banner UI Fix.
* Category Page Banner UI Update.

= 1.0.2 - 30.08.2016 =
* Check  WordPress and WooCommerce compatibility

= 1.0.1 - 8.08.2016 =
* Fixes - Minor bug solved