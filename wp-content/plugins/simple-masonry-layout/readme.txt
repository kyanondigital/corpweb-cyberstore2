=== Plugin Name ===
Contributors: razzu
Tags: masonry,masonry plugin,jquery masonry,masonry layout,grid layout,shortcode,blog masonry,gallery masonry,custom post type masonry,grid loading effect,jquery popup gallery,animation gallery,scroll masonry,responsive jquery popup gallery masonry
Requires at least: 4.2
Tested up to: 4.6.1
Stable tag: 1.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

With simple shortcode, Masonry Layout in action.

== Description ==

A simple  Masonry  Layout plugin. with simple shortcode, You can add Masonry Layout For Posts , custom post types .And Even Masonry Layout Gallery For Posts and custom post type. Very easy to Use with very simple shortcodes. Better Grid View of Posts, custom post types.

This plugin will modify your Blogs to better looking advance Masonry(Grid) Layout with use of simple available shortcodes.

* Masonry Layout Blog.
* Masonry Layout custom post type.
* Masonry Layout Gallery For Posts 
* Mansory Layout with Simple Responsive Gallery Popup.
* Simple Shortcode can be used for Masonry Layout.
* Shortcode can be either used in editor or can be used in template files.

== Installation ==

1. Upload "simple-masonry-layout" folder to the "/wp-content/plugins/" directory
2. Activate the plugin through the Plugins menu into WordPress admin area
3. Configure your neccessary settings in Admin-panel `Settings -> Simple Masonry Layout`.
4. Add Shortcode [simple_masonry] in editor. For using in template files, &lt;? echo do_shortcode('[simple_masonry]'); ?&gt;. Other shortcode are available in plugin settings Panel. i.e :- `Settings-> Simple Masonry Layout`.

== Frequently Asked Questions ==

= Is plugin compatible with every theme ? =

Yes, this plugin is 100% compatible with every free, custom and paid theme. You can even use this plugin to modify default Wordpress theme into better looking Masonry(Grid) Layout theme with simple available shortcodes.

= Is Mansory Layout Responsive ? =

Yes, Mansory Layout is Responsive. It has Responsive Jquery popup too.



 
== Screenshots ==

1. Screenshot displaying Simple Masonry Layout of Posts
2. Screenshot displaying Simple Masonry Layout Gallery
2. Screenshot displaying Simple Responsive Gallery Popup.


== Changelog ==


= 1.1 =
* Added Grid Loading Effect/Animation on scroll
* Added Simple Responsive Jquery Popup
* Added Post Title URL link to Gallery
* Added Post Title URL setting

= 1.0 =
* This is initial version of the plugin.




